<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        //
        $orders = DB::table('sweetwater_test')->select('orderid', 'comments')->get();
        $candy_comments = [];
        $call_comments = [];
        $signature_comments = [];
        $referral_comments = [];
        $misc_comments = [];
        
        foreach($orders as $order){
            // Split comment
            $pieces = explode("Expected Ship Date:", $order->comments);

            // Save shipdate
            if (count($pieces) > 1){
                $time = strtotime($pieces[1]);
                $date = date('Y-m-d', $time);

                DB::table('sweetwater_test')->where('orderid', $order->orderid)->update(['shipdate_expected' => $date]);
            }

            // Sort comment type
            if ( preg_match("/signature/i", $pieces[0]) ){
                # Signature
                array_push($signature_comments, $pieces[0]);
            } elseif ( preg_match("/call |llámame|comunicarse /i", $pieces[0]) ){
                # Call / Don't Call
                array_push($call_comments, $pieces[0]);
            } elseif ( preg_match("/candy|tootsie|smarties|taffy/i", $pieces[0]) ){
                # Candy
                array_push($candy_comments, $pieces[0]);
            } elseif ( preg_match("/referred|referral|heard of|heard about|told me about|fan|friend |internet/i", $pieces[0]) ){
                # Referrals
                array_push($referral_comments, $pieces[0]);
            } else {
                # Miscellaneous
                array_push($misc_comments, $pieces[0]);
            }
        }
        
        return view('welcome', compact('candy_comments', 'call_comments', 'signature_comments', 'referral_comments', 'misc_comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        // Use this to update the expected shipdate times
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
