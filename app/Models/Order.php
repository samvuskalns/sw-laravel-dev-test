<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sweetwater_test';

    /**
     * The primary key associated with the table.
     * 
     * @var string
     */
    protected $primaryKey = 'order_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Format individual date and datetime columns.
     * 
     * @var string
     */
     protected $casts = [
        'shipdate_expected' => 'datetime:Y-m-d H:i:s',
    ];

}


